#!/usr/bin/env bash

# By Shaojun Xie

## Please change the path accordingly.
Path2ViewBS=$(dirname $(readlink -e ../))
echo $Path2ViewBS
PATH=$PATH:$Path2ViewBS

which ViewBS 2>/dev/null && echo "Found ViewBS in the PATH. " || { echo >&2 "ViewBS is not in the path. Please add  Aborting."; exit 1; }

[ -f bis_WT.tab.gz ] && echo "Found bis_WT.tab.gz" || { echo >&2 "The Input file is different from the shell script.\n Aborting."; exit 1; }

#echo  "Convert brat results to bismark report\n";
#./memusg perl $Path2ViewBS/lib/scripts/brat2bismark.pl brat_meth_output.txt 1 brat_meth4bismark_report.txt

echo  "BisNonvRate\n";
./memusg perl $Path2ViewBS/ViewBS BisNonConvRate --sample bis_WT.tab.gz,WT --sample bis_cmt23.tab.gz,cmt23 --sample bis_cmt2-3.tab.gz,cmt2-3 --sample bis_drm12cmt23.tab.gz,drm12cmt23 --sample bis_drm12cmt2.tab.gz,drm12cmt2 --outdir BisNonConvRate --prefix cmt2_proj_allsam --chr chrC --height 10 --width 14

echo "MethCoverage\n";
./memusg perl $Path2ViewBS/ViewBS MethCoverage --reference ../testdata/TAIR10_chr_all.fasta --sample bis_WT.tab.gz,WT --sample bis_cmt23.tab.gz,cmt23 --sample bis_cmt2-3.tab.gz,cmt2-3 --sample bis_drm12cmt23.tab.gz,drm12cmt23 --sample bis_drm12cmt2.tab.gz,drm12cmt2 --outdir MethCoverage --prefix cmt2_proj_allsam

echo "MethLevDist\n"
./memusg perl $Path2ViewBS/ViewBS MethLevDist --sample bis_WT.tab.gz,WT --sample bis_cmt23.tab.gz,cmt23 --sample bis_cmt2-3.tab.gz,cmt2-3 --sample bis_drm12cmt23.tab.gz,drm12cmt23 --sample bis_drm12cmt2.tab.gz,drm12cmt2 --outdir MethLevDist --prefix cmt2_proj_allsam --binMethLev 0.1 --height 10 --width 14

echo "GlobalMethLev\n"
./memusg perl $Path2ViewBS/ViewBS GlobalMethLev --sample bis_WT.tab.gz,WT --sample bis_cmt23.tab.gz,cmt23 --sample bis_cmt2-3.tab.gz,cmt2-3 --sample bis_drm12cmt2.tab.gz,drm12cmt2 --sample bis_drm12cmt23.tab.gz,drm12cmt23 --outdir MethGlobal --prefix cmt2_proj_allsam

echo "MethGeno"
./memusg perl $Path2ViewBS/ViewBS MethGeno --genomeLength ../testdata/TAIR10_chr_all.fasta.fai --sample bis_WT.tab.gz,WT --sample bis_cmt23.tab.gz,cmt23 --sample bis_cmt2-3.tab.gz,cmt2-3 --sample bis_drm12cmt2.tab.gz,drm12cmt2 --sample bis_drm12cmt23.tab.gz,drm12cmt23 --prefix bis_geno_sample --context CHG --outdir MethGeno

echo "Test MethOverRegion"
./memusg perl $Path2ViewBS/ViewBS MethOverRegion --region TAIR10_GFF3_genes_chr1.bed --sample bis_WT.tab.gz,WT --sample bis_cmt23.tab.gz,cmt23 --sample bis_cmt2-3.tab.gz,cmt2-3 --sample bis_drm12cmt2.tab.gz,drm12cmt2 --sample bis_drm12cmt23.tab.gz,drm12cmt23 --prefix bis_gene_chr1_sample --context CG --outdir MethOverRegion

./memusg perl $Path2ViewBS/ViewBS MethOverRegion --region TAIR10_GFF3_genes_chr1.bed --sample bis_WT.tab.gz,WT --sample bis_cmt23.tab.gz,cmt23 --sample bis_cmt2-3.tab.gz,cmt2-3 --sample bis_drm12cmt2.tab.gz,drm12cmt2 --sample bis_drm12cmt23.tab.gz,drm12cmt23 --prefix bis_gene_chr1_sample --context CHH --outdir MethOverRegion --regionName TE

./memusg perl $Path2ViewBS/ViewBS MethOverRegion --region TAIR10_Transposable_Elements.chr1.bed --sample bis_WT.tab.gz,WT --sample bis_cmt23.tab.gz,cmt23 --sample bis_cmt2-3.tab.gz,cmt2-3 --sample bis_drm12cmt2.tab.gz,drm12cmt2 --sample bis_drm12cmt23.tab.gz,drm12cmt23 --prefix bis_TE_chr1_sample --context CHG --outdir MethOverRegion --regionName TE

./memusg perl $Path2ViewBS/ViewBS MethOverRegion --region TAIR10_Transposable_Elements.chr1.bed --sample bis_WT.tab.gz,WT --sample bis_cmt23.tab.gz,cmt23 --sample bis_cmt2-3.tab.gz,cmt2-3 --sample bis_drm12cmt2.tab.gz,drm12cmt2 --sample bis_drm12cmt23.tab.gz,drm12cmt23 --prefix bis_TE_chr1_sample --context CHH --outdir MethOverRegion --regionName TE

echo "Test MethOverRegion: 5 Ranks\n"
./memusg perl $Path2ViewBS/ViewBS MethOverRegion --sample file:sampl_info_tab.txt --prefix bis_gene_5rank --context CG --outdir MethOverRegion

echo "Test MethHeatmap\n"
./memusg perl $Path2ViewBS/ViewBS MethHeatmap --region CHG_hypo_DMR_drm12cmt23_to_WT.txt --sample bis_WT.tab.gz,WT --sample bis_drm12cmt23.tab.gz,drm12cmt23 --sample bis_cmt23.tab.gz,cmt23 --sample bis_cmt2-3.tab.gz,cmt2-3 --sample bis_drm12cmt2.tab.gz,drm12cmt2 --prefix CHG_hypo_DMR_drm12cmt23_to_WT --context CHG --outdir MethHeatmap

echo "Test MethOneRegion"
./memusg perl $Path2ViewBS/ViewBS MethOneRegion --region chr5:19497000-19499600 --sample bis_WT.tab.gz,WT --sample bis_cmt23.tab.gz,cmt23 --outdir MethOneRegion --prefix chr5_19497000-19499600 --context CHG

