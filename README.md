This repository is for the test data of ViewBS. 

## Install ViewBS first. 

Download the file via the command line below or just click the download link. 

```
## Download ViewBS
git clone https://github.com/xie186/ViewBS.git
cd ViewBS/
```
Then install the dependecies (https://github.com/xie186/ViewBS/tree/dev#install).

## Test ViewBS

Download the file via the command line below or just click the download link.

```
git clone git@gitlab.com:BS-seq/ViewBS_testdata.git
cd testdata/
sh test_ViewBS.sh
```

> Note: to run `test_ViewBS.sh`, the directory (ViewBS_testdata) should be in the folder of `ViewBS`. Otherwise, the user needs to change `Path2ViewBS` accordingly. 

